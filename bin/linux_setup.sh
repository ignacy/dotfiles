#!/bin/sh
#
#
if type setxkbmap &> /dev/null; then
   setxkbmap pl -variant colemak
   setxkbmap -option "altwin:swap_lalt_lwin,caps:ctrl_modifier"
fi
