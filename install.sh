#!/bin/bash

set -e

if ! command -v stow &> /dev/null
then
    echo "Install stow first"
    exit
fi

stow .

rm ~/install.sh

echo "All done"

