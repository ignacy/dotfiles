require 'irb/ext/save-history'
#require 'pry'

IRB.conf[:AUTO_INDENT] = true
IRB.conf[:SAVE_HISTORY] = 2000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-history"
IRB.conf[:PROMPT_MODE] = :SIMPLE

#Pry.config.pager = false
#Pry.config.correct_indent = false

#if ENV['TERM'] == 'emacs'
#  Pry.config.color = false
#  Pry.config.pager = false
#  Pry.config.auto_indent = false
#end

