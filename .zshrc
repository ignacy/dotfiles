# Set custom prompt
bindkey -v

export NVM_LAZY_LOAD=true

source ~/.zsh/custom/nvm_hack

export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

export AWS_USER="ignacy.moryc"
export MFA_ARM="arn:aws:iam::982696199642:mfa/ignacy.moryc"

export PYTHON=python3


export KAFKA_HOME="/usr/local/Cellar/kafka/3.2.0"
export HELIX_RUNTIME="$HOME/code/helix/runtime"
export COLORTERM=truecolor
# Add paths
export PATH=/usr/local/sbin:/usr/local/bin:${PATH}
export PATH="$HOME/bin:$PATH"
export PATH="$HOME/.luarocks/bin:$PATH"
export PATH="$HOME/.emacs.d/bin:$PATH"

export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
#export PATH="$HOME/.rbenv/shims:$PATH"
export PATH="$HOME/.skim/bin:$PATH"
export PATH="$PATH:/usr/local/go/bin"
export PATH="$HOME/go/bin:$PATH"

alias smartaws=". ~/bin/smartaws"


export WATCHED_REPOS_NAMES="api,engineering_docs,rswag,tag-connector-service,smart-kafka-rb,payments-connector-service,smart-tx,smart-bus,rubocop-smart"

# Colorize terminal
alias ls='ls -G'
alias ll='ls -lG'
export LSCOLORS="ExGxBxDxCxEgEdxbxgxcxd"
export GREP_OPTIONS="--color"

pcs() {
  cd ~/code/smart/payments-connector-service/
}

rcopDiff() {
  git diff --name-only --cached --diff-filter=ACMRTB | grep -E '.rb$' | xargs bundle exec rubocop -A
}


## ON LINUX
##
### Install it with the available package manager.
# Add to e.g: ~/.bash_profile to load on startup
#alias pbcopy='xclip -selection clipboard'
#alias pbpaste='xclip -selection clipboard -o'



alias search="rg" # Nigdy nie pamietam czy mam grep, rg, ag..


alias d='git diff'
alias s="git status"
alias gcm="git commit -m"
alias gcnm="git commit -n -m"
alias gup="git pull --rebase"
alias grbi="git rebase --interactive"
alias gpf="git push --force-with-lease"
alias gsl="git lg"
alias gad="git add ."
alias bex="bundle exec"
alias bes="bundle exec rspec spec"
alias speedtest="curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -"

garr() {
  gad && rcopDiff 
}


run_branch_specs() {
  git diff --name-only main | grep "_spec.rb" | xargs bundle exec rspec
}



# Nicer history
export HISTSIZE=100000
export HISTFILE="$HOME/.history"
export SAVEHIST=$HISTSIZE

setopt EXTENDED_HISTORY
setopt HIST_VERIFY
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Dont record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Dont record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Dont write duplicate entries in the history file.

setopt inc_append_history
setopt share_history

# Use vim as the editor
export EDITOR=nvim

# Use C-x C-e to edit the current command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line

# By default, zsh considers many characters part of a word (e.g., _ and -).
# Narrow that down to allow easier skipping through words via M-f and M-b.
export WORDCHARS='*?[]~&;!$%^<>'

export HOMEBREW_NO_ANALYTICS=1

if [ -f ~/.fzf/shell/key-bindings.zsh ]; then
source ~/.fzf/shell/key-bindings.zsh
#   fzf_key_bindings
fi

if [ -f ~/.fzf.zsh ]; then
source ~/.fzf.zsh
#   fzf_key_bindings
fi

if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
source /usr/share/doc/fzf/examples/key-bindings.zsh
fi

if type direnv &> /dev/null; then
eval "$(direnv hook zsh)"
fi

reconnect_trackpad() {
  echo -n "none" | sudo tee /sys/bus/serio/devices/serio1/drvctl &&
    echo -n "reconnect" | sudo tee /sys/bus/serio/devices/serio1/drvctl
}


export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"


function git-changed() {
  # diff-filter - select types of files. we exclude removed ones
  git diff --name-only --cached --diff-filter=ACMRTB
}

function rubocop-pr() {
  git-changed | grep -E '.rb$' | xargs bundle exec rubocop -A
}

function review () {
  hub pr checkout $1 && emp
}

function gco() {
  git checkout $(git for-each-ref refs/heads/ --format='%(refname:short)' | fzf)
}



function smartlog () {
  DATE=$(date +'* %d-%m-%Y %H:%M |');
  PROJECT="${2:-smart}";
  TIMETAKEN="${3:-1h}";
  echo "$DATE $PROJECT | $TIMETAKEN | $1" >> ~/org/smartlog.md

  tail -n 10 ~/org/smartlog.md
}

alias hh='hx $(rg --files | fzf)'

alias vv='nvim $(fzf)'
alias ff='nvim $(fzf)'


if type brew &>/dev/null; then
  FPATH=/usr/local/share/zsh/site-functions:$FPATH
  #export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"
fi

#if [ -f /usr/local/opt/asdf/libexec/asdf.sh ]; then
#  source /usr/local/opt/asdf/libexec/asdf.sh
if [ -f ~/.asdf/asdf.sh ]; then
  source ~/.asdf/asdf.sh
fi

fpath=(~/.zsh/completion $fpath)
fpath+=($HOME/.zsh/pure)
autoload -U promptinit; promptinit
prompt pure

# Initialize completion
autoload -Uz compinit
for dump in ~/.zcompdump(N.mh+24); do
  compinit
done
compinit -C

unsetopt menu_complete
unsetopt flowcontrol
setopt auto_menu
setopt complete_in_word
setopt complete_aliases
setopt always_to_end
setopt auto_pushd

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

alias dc="docker-compose"
#compdef _docker-compose dc=docker-compose
bindkey '^I' complete-word

# Some emacs keybindings won't hurt nobody
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line


#############
## PRIVATE ##
#############
# Include private stuff that's not supposed to show up in the dotfiles repo
local private="${HOME}/.zsh/private.sh"
if [ -e ${private} ]; then
. ${private}
fi

#####
# Heroku #
####
alias heat_logs="heroku logs --tail -a=heat-api-production"
alias heat_cons="heroku run rails console --tail -a=heat-api-production"


export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

eval "$(rbenv init - --no-rehash)"

# heroku autocomplete setup
#HEROKU_AC_ZSH_SETUP_PATH=/Users/ignacy.moryc/Library/Caches/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;
