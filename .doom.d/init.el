;;; init.el -*- lexical-binding: t; -*-
;; Copy this file to ~/.doom.d/init.el or ~/.config/doom/init.el ('doom install'
;; will do this for you). The `doom!' block below controls what modules are
;; enabled and in what order they will be loaded. Remember to run 'doom refresh'
;; after modifying it.
;;
;; More information about these modules (and what flags they support) can be
;; found in modules/README.org.

(doom! :input

       :completion
       company           ; the ultimate code completion backend
       (vertico +icons) ;; - ciagle ma problemy na tramp

       :ui
       doom              ; what makes DOOM look the way it does
       doom-dashboard    ; a nifty splash screen for Emacs
       ;; (emoji +unicode)
       ;;(ligatures +extra)
       ;;nav-flash                    ; blink the current line after jumping
       ;;doom-quit         ; DOOM quit-message prompts when you quit Emacs
       (modeline +light)          ; snazzy, Atom-inspired modeline, plus API
       ophints           ; highlight the region an operation acts on
       (popup            ; tame sudden yet inevitable temporary windows
        +defaults)       ; default popup rules
       workspaces
       zen

       :editor
       (evil +everywhere); come to the dark side, we have cookies
       format
       snippets
       ;; lispy

       :emacs
       electric          ; smarter, keyword-based electric-indent
       ibuffer
       vc                ; version-control and Emacs, sitting in a tree
       undo

       :term
       vterm

       :checkers
       ;; grammar
       spell
       (syntax +childframe)

       :tools
       ;;direnv
       docker
       editorconfig      ; SLOW ON TRAMP
       (eval +overlay)              ; run code, run (also, repls)
       lookup           ; helps you navigate your code and documentation
       lsp
       magit
                                        ;a git porcelain for Emacs

       :lang
       ;;data              ; config/data formats
       emacs-lisp        ; drown in parentheses
       ;;go
       ;;(haskell +dante) ; a language that's lazier than I am
       ;;javascript
       ;;(lua +lsp)               ; one-based indices? one-based indices
       ;;racket
       (rust +lsp)
       ;;markdown          ; writing docs for people to ignore
       ;;org
       (ruby +rbenv +lsp)
       ;;(scheme +mit)
       sh                ; she sells {ba,z,fi}sh shells on the C xor
       (web)              ; the tubes
       yaml ;;+lsp)

       :os
       (:if IS-MAC macos)  ; improve compatibility with macOS
       ;;tty               ; improve the terminal Emacs experience

       :email
       :app
       :config
       (default +bindings +smartparens))
