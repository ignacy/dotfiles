;; config.el -*- lexical-binding: t; -*-
;;
;; Place your private configuration here
;;
;;
(defun im/rename-file-and-buffer ()
  "Rename the current buffer and file it is visiting."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (message "Buffer is not visiting a file!")
      (let ((new-name (read-file-name "New name: " filename)))
        (cond
         ((vc-backend filename) (vc-rename-file filename new-name))
         (t
          (rename-file filename new-name t)
          (set-visited-file-name new-name t t)))))))


(use-package abbrev
  :init
  (setq-default abbrev-mode t)
  ;; a hook funtion that sets the abbrev-table to org-mode-abbrev-table
  ;; whenever the major mode is a text mode
  (defun tec/set-text-mode-abbrev-table ()
    (if (derived-mode-p 'text-mode)
        (setq local-abbrev-table org-mode-abbrev-table)))
  :commands abbrev-mode
  :hook
  (abbrev-mode . tec/set-text-mode-abbrev-table)
  :config
  (setq abbrev-file-name "~/dotfiles/abbrevations")
  (setq save-abbrevs 'silently))

(use-package-hook! rubocop
  :pre-init
  (progn
    ;;(define-key enh-ruby-mode-map (kbd "C-c C-f") 'rubocop-autocorrect-current-file)
    (defun rubocop-ensure-installed () )
    ;;(defun rubocop-bundled-p () nil)))
    ))


;;(setq flycheck-display-errors-delay 1.5)
(setq-default flycheck-disabled-checkers '(ruby-reek ruby-rubocop))

(setq-default history-length 1000)
(setq-default prescient-history-length 1000)
(setq-default history-length 1000)
(setq-default prescient-history-length 1000)


(setq-default warning-minimum-level :error)
(setq-default warning-minimum-log-level :error)

;;(global-hl-line-mode 1)

;; (use-package! fancy-dabbrev
;;   :hook
;;   (prog-mode . fancy-dabbrev-mode)
;;   (org-mode . fancy-dabbrev-mode)
;;   :config
;;   ;; (setq fancy-dabbrev-preview-delay 0.1)
;;   (setq fancy-dabbrev-preview-context 'before-non-word)
;;   ;; Let dabbrev searches ignore case and expansions preserve case:
;;   (setq dabbrev-case-distinction nil)
;;   (setq dabbrev-case-fold-search t)
;;   (setq dabbrev-case-replace nil)
;;   (add-hook 'minibuffer-setup-hook (lambda () (fancy-dabbrev-mode 0)))
;;   (add-hook 'minibuffer-exit-hook (lambda () (fancy-dabbrev-mode 1))))

(use-package! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

(setq display-line-numbers-type 'normal) ;; or relative
(define-key isearch-mode-map (kbd "C-g") 'isearch-cancel) ;; isearch-abort is deafult and it sucks!!

(defun my-keyboard-quit()
  "Escape the minibuffer or cancel region consistently using 'Control-g'.
Normally if the minibuffer is active but we lost focus (say, we clicked away or set the cursor into another buffer)
we can quit by pressing 'ESC' three times. This function handles it more conveniently, as it checks for the condition
of not beign in the minibuffer but having it active. Otherwise simply doing the ESC or (keyboard-escape-quit) would
brake whatever split of windows we might have in the frame."
  (interactive)
  (if (not(window-minibuffer-p (selected-window)))
      (if (or mark-active (active-minibuffer-window))
          (keyboard-escape-quit))
    (keyboard-quit)))

(define-key global-map (kbd "C-g") 'my-keyboard-quit)

(setq evil-complete-previous-func 'hippie-expand)
(dolist (f '(try-expand-line try-expand-list try-complete-file-name-partially try-complete-file-name))
  (setq hippie-expand-try-functions-list (delete f hippie-expand-try-functions-list)))

(add-to-list 'hippie-expand-try-functions-list 'try-complete-file-name-partially t)
(add-to-list 'hippie-expand-try-functions-list 'try-complete-file-name t)

;; (add-hook 'prog-mode-hook 'idle-highlight-in-visible-buffers-mode)
;; (setq highlight-indent-guides-method 'character)
;;
(cond ((string= (system-name) "ML-1066") (setq doom-font (font-spec :family "SF Mono" :size 14)))
      ((string= (system-name) "pop-os") (setq doom-font (font-spec :family "Monaco" :size 23)))
      ((string= (system-name) "eckhart-mac.local") (setq doom-font (font-spec :family "Monaco" :size 16)))
      (t (setq doom-font (font-spec :family "Monaco" :size 20))))

(global-set-key (kbd "C-x k") 'kill-this-buffer)
(setq mac-command-modifier 'meta)
(setq mac-right-option-modifier nil)


(defun rails-console (dir)
  "Run Rails console in DIR."
  (interactive (list (inf-ruby-console-read-directory 'rails)))
  (let* ((default-directory (file-name-as-directory dir))
         )
    (inf-ruby-console-run
     (concat "docker-compose exec backend rails console"
             ;; Note: this only has effect in Rails < 5.0 or >= 5.1.4
             ;; https://github.com/rails/rails/pull/29010
             (when (inf-ruby--irb-needs-nomultiline-p)
               " -- --nomultiline"))
     "rails")))

;; (after! company
;;   ;;(setq company-minimum-prefix-length 2)
;;   (setq company-show-numbers t)
;;   (setq company-idle-delay 0)
;;   (setq company-require-match 'never)
;;   (add-hook 'evil-normal-state-entry-hook #'company-abort))

(after! projectile
  (setq projectile-project-root-files-bottom-up
        (remove ".git" projectile-project-root-files-bottom-up)))

(setq ispell-dictionary "en,pl")

(setq evil-snipe-scope 'buffer)

(add-hook 'prog-mode-hook
          (lambda ()
            (rainbow-delimiters-mode-enable)))

(use-package lambda-themes
 :custom
  (lambda-themes-set-italic-comments t)
  (lambda-themes-set-italic-keywords t)
  (lambda-themes-set-variable-pitch t))

(add-to-list 'custom-theme-load-path "~/.doom.d/themes/")

(load-theme 'hima t)


(setq avy-all-windows t)

(map! :leader
      :desc "Avy jump timer"
      "j" #'evil-avy-goto-char-timer)

(map! :leader
      :desc "Avy jump timer"
      "SPC" #'evil-avy-goto-char-timer)

(rbenv-use "3.0.3")
