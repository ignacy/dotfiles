;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)
(package! rubocop)

(package! tree-sitter)
(package! tree-sitter-langs)

(package! openapi-yaml-mode :recipe (:host github :repo "esc-emacs/openapi-yaml-mode"))
(package! ob-typescript)
(package! projectile-rails)
;;(package! hl-line :disable t)
(package! evil-motion-trainer :recipe (:repo "https://github.com/martinbaillie/evil-motion-trainer"))

(package! lambda-themes :recipe (:repo "https://github.com/lambda-emacs/lambda-themes"))

(package! cyberpunk-theme)
(package! ef-themes)

(package! emojify)
;;(ignore-errors (package! harpoon))
