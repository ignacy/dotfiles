set fish_greeting

abbr -a g git
abbr -a dc "docker-compose"
abbr -a gad "git add ."
abbr -a bex "bundle exec"
abbr -a gcm "git commit -m"
abbr -a gup "git pull --rebase"
abbr -a gsl "git log"
abbr -a dotf "cd ~/dotfiles"
abbr -a s "git status"
abbr -a fmtswg "--format Rswag::Specs::SwaggerFormatter"

if type -q setxkbmap
	setxkbmap pl -variant colemak
	setxkbmap -option caps:ctrl_modifier
end

set -x PATH /usr/local/sbin $PATH
set -x PATH /usr/local/bin $PATH
set -x PATH $HOME/bin $PATH
set -x PATH ~/.local/bin $PATH
set -x PATH ~/.emacs.d/bin $PATH
set -x PATH $HOME/.asdf/shims $PATH


set -x LANG "en_US.UTF-8"

set -x FZF_DEFAULT_COMMAND 'fd --type f --exclude .git'
set -x FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"

alias fzfi='rg --files --hidden --follow -g "!{node_modules,.git}" | fzf'
alias vv='nvim (fzfi)'
alias nv=nvim
alias kk='kak (fzfi)'
alias d='git diff'
alias gcm="git commit -m"
alias gup="git pull --rebase"
alias gpf="git push --force-with-lease"

alias fconf="nvim ~/.config/fish/config.fish"


if test (uname) = "Darwin"
	set -x PATH /usr/local/opt/emacs-plus@28/bin $PATH
end

if test -e ~/.asdf/asdf.fish
	source ~/.asdf/asdf.fish
end

if test -e ~/.fzf/shell/key-bindings.fish
	source ~/.fzf/shell/key-bindings.fish
	fzf_key_bindings
end

set -U fish_user_paths $HOME/.cargo/bin $fish_user_paths

function reconnect_trackpad
  echo -n "none" | sudo tee /sys/bus/serio/devices/serio1/drvctl &&
    echo -n "reconnect" | sudo tee /sys/bus/serio/devices/serio1/drvctl
end


if command -v exa > /dev/null
	abbr -a l 'exa'
	abbr -a ls 'exa'
	abbr -a ll 'exa -l'
	abbr -a lll 'exa -la'
else
	abbr -a l 'ls'
	abbr -a ll 'ls -l'
	abbr -a lll 'ls -la'
end




if test -e ~/.config/fish/functions/theme-pure/conf.d/pure.fish
	# THEME PURE #
	set fish_function_path ~/.config/fish/functions/theme-pure/functions/ $fish_function_path
	source ~/.config/fish/functions/theme-pure/conf.d/pure.fish
end


if test -e /opt/homebrew/bin/brew
	eval (/opt/homebrew/bin/brew shellenv)
end

function nvm
   bass source (brew --prefix nvm)/nvm.sh --no-use ';' nvm $argv
end

set -x NVM_DIR ~/.nvm
nvm use default --silent
