# Configuration for Alacritty, the GPU enhanced terminal emulator

# Any items in the `env` entry below will be added as
# environment variables. Some entries may override variables
# set by alacritty it self.
env:
  # TERM env customization.
  #
  # If this property is not set, alacritty will set it to xterm-256color.
  #
  # Note that some xterm terminfo databases don't declare support for italics.
  # You can verify this by checking for the presence of `smso` and `sitm` in
  # `infocmp xterm-256color`.
  TERM: xterm-256color

window:
  dynamic_title: true
  # Window dimensions in character columns and lines
  # Falls back to size specified by window manager if set to 0x0.
  # (changes require restart)
  dimensions:
    columns: 120
    lines: 24

  # Adds this many blank pixels of padding around the window
  # Units are physical pixels; this is not DPI aware.
  # (change requires restart)
  padding:
    x: 2
    y: 2

  # Window decorations
  # Setting this to false will result in window without borders and title bar.
  decorations: full

# When true, bold text is drawn using the bright variant of colors.
draw_bold_text_with_bright_colors: true

# * file:///usr/share/doc/sontconfig/fontconfig-user.html
# Font configuration (changes require restart)
# Font configuration (changes require restart)
font:
  # Normal (roman) font face
  normal:
    #    Font family
    # Default:
    #   - (macOS) Menlo
    #   - (Linux) monospace
    #   - (Windows) Consolas
    family: Monaco

  # Bold font face
  bold:
    # Font family
    #
    # If the bold family is not specified, it will fall back to the
    # value specified for the normal font.
    family: Monaco

  # Italic font face
  italic:
    # Font family
    #
    # If the italic family is not specified, it will fall back to the
    # value specified for the normal font.
    family: Monaco

  # Point size
  size: 16

  # Offset is the extra space around each character. `offset.y` can be thought of
  # as modifying the line spacing, and `offset.x` as modifying the letter spacing.
  offset:
    x: 0
    y: 0

  # Glyph offset determines the locations of the glyphs within their cells with
  # the default being at the bottom. Increasing `x` moves the glyph to the right,
  # increasing `y` moves the glyph upwards.
  glyph_offset:
    x: 0
    y: 0

# Should display the render timer
# Visual Bell
#
# Any time the BEL code is received, Alacritty "rings" the visual bell. Once
# rung, the terminal background will be set to white and transition back to the
# default background color. You can control the rate of this transition by
# setting the `duration` property (represented in milliseconds). You can also
# configure the transition function by setting the `animation` property.
#
# Possible values for `animation`
# `Ease`
# `EaseOut`
# `EaseOutSine`
# `EaseOutQuad`
# `EaseOutCubic`
# `EaseOutQuart`
# `EaseOutQuint`
# `EaseOutExpo`
# `EaseOutCirc`
# `Linear`
#
# To completely disable the visual bell, set its duration to 0.
#
bell:
  animation: EaseOutExpo
  duration: 0

# Mouse bindings
#
# Currently doesn't support modifiers. Both the `mouse` and `action` fields must
# be specified.
#
# Values for `mouse`:
# - Middle
# - Left
# - Right
# - Numeric identifier such as `5`
#
# Values for `action`:
# - Paste
# - PasteSelection
# - Copy (TODO)
mouse_bindings:
  - { mouse: Middle, action: PasteSelection }

mouse:
  # Click settings
  #
  # The `double_click` and `triple_click` settings control the time
  # alacritty should wait for accepting multiple clicks as one double
  # or triple click.
  double_click: { threshold: 300 }
  triple_click: { threshold: 300 }

  # Faux Scrollback
  #
  # The `faux_scrollback_lines` setting controls the number
  # of lines the terminal should scroll when the alternate
  # screen buffer is active. This is used to allow mouse
  # scrolling for applications like `man`.
  #
  # To disable this completely, set `faux_scrollback_lines` to 0.
  faux_scrolling_lines: 1

selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>"
  # When set to `true`, selected text will be copied to the primary clipboard.
  save_to_clipboard: true

# Live config reload (changes require restart)
live_config_reload: true

key_bindings:
  - { key: F11, action: ToggleFullscreen }
    
# Colors (Iceberg Light)
colors:
  # Default colors
  primary:
    background: '#e8e9ec'
    foreground: '#33374c'

  # Normal colors
  normal:
    black:   '#dcdfe7'
    red:     '#cc517a'
    green:   '#668e3d'
    yellow:  '#c57339'
    blue:    '#2d539e'
    magenta: '#7759b4'
    cyan:    '#3f83a6'
    white:   '#33374c'

  # Bright colors
  bright:
    black:   '#8389a3'
    red:     '#cc3768'
    green:   '#598030'
    yellow:  '#b6662d'
    blue:    '#22478e'
    magenta: '#6845ad'
    cyan:    '#327698'
    white:   '#262a3f'
